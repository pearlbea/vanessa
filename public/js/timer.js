var Tock = require('tocktimer'),
    $ = require('jquery'),
    postal = require('postal'),
    timerChannel = postal.channel('timer'),
    ctrl = require('./controller'),
    i = 0,
    value,
    colors = [
        'green', 'aqua', 'violet', 'rust'
    ];

'use strict';

var exerciseTimer = new Tock({
    countdown: true,
    callback: function () {
        $('body')
            .find('.countdown')
            .fadeOut(function(){
                $(this).text(value--).fadeIn();
            });
    },
    complete: function(){
        timerChannel.publish('timer.exerciseDone');
    }
});

var colorTimer = new Tock({
    countdown: true,
    callback: function(){
        $('body')
            .find('#main')
            .addClass(colors[i])
            .removeClass(colors[i-1]);
        i++;
    },
    complete: function(){
        i = 0;
    }
});

exports.startExerciseTimer = function (data) {
    var int, totalTime;

    if(data.measure === 'reps'){
        int = Number(data.timePerRep) * 1000;
        totalTime = int * data.value;
        value = data.value;
    }else{
        int = 1000;
        totalTime = data.value * 1000;
        value = Number(data.value);
    }

    exerciseTimer.interval = int;
    exerciseTimer.start(totalTime);

    colorTimer.interval = totalTime/4;
    colorTimer.start(totalTime);
};

exports.togglePauseExerciseTimer = function () {
    exerciseTimer.pause();
    colorTimer.pause();
};

exports.stopExerciseTimer = function () {
    $('body').find('#main').removeClass().addClass('green');
    exerciseTimer.stop();
    exerciseTimer.reset();
    colorTimer.stop();
    colorTimer.reset();
};


