/**
 * public/js/stores/workout-store
 * @summary fetches and stores workout data
 * @type {Reflux|exports|module.exports}
 */

'use strict';

var Reflux = require('reflux'),
    data = require('../data/workouts'),
    Actions = require('../actions'),
    _ = require('underscore');

module.exports = Reflux.createStore({

    listenables: [Actions],

    getUserWorkouts: function(){
        data.getUserWorkouts()
            .then(function(res){
                this.workouts = res;
                this.returnWorkouts();
            }.bind(this));
    },

    find: function(id){
        return _.findWhere(this.workouts.workouts, {_id: id});
    },

    getWorkout: function(id){
        var workout = this.find(id);
        if(workout){
            this.workout = workout;
            this.returnWorkout()
        }
    },

    returnWorkouts: function(){
        this.trigger('change', this.workouts);
    },

    returnWorkout: function(){
        this.trigger('change', this.workout);
    }

});
