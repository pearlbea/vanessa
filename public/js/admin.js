/**
 * public/js/admin.js
 */

'use strict';

var $ = require('jquery'),
    React = require('react'),
    postal = require('postal'),
    exerciseChannel = postal.channel('exercise'),
    exercises = require('./data/exercises'),
    workouts = require('./data/workouts'),
    WorkoutBuilder = require('./components/admin/WorkoutBuilder.jsx');


exerciseChannel.subscribe('exercise.*', function(data, env){
    console.log(data, env.topic)
});

function getExercises(){
    exercises.getExercises();
}

function initComponent(){
    React.render(
        <WorkoutBuilder />,
        document.getElementById('container')
    );
};

exports.initModule = function(){

    $('.admin').on('click', '.delete-row', function(){
        $(this).parents('tr').fadeOut().remove();
    });

    $('.admin').on('change', 'input[type="checkbox"]', function(){
        var active = $(this).prop('checked'),
            id = $(this).attr('data-id');
        workouts.activateWorkout(id, active);
    });

    $('.add-exercise').on('click', function(e){
        e.preventDefault();
       var $tr = $('tr:last-of-type'),
           $clone = $tr.clone();
        $clone.find(':text').val('');
        $tr.after($clone);

    });

    if($('#container').length){
        initComponent();
    }

};