/**
 * public/js/data/exercises
 */

'use strict';

var $ = require('jquery'),
    Q = require('q'),
    postal = require('postal'),
    exerciseChannel = postal.channel('exercise');

function getExercises(){
    return Q($.get('/api/exercises'));
}

exports.getExercises = function(){
    getExercises()
        .then(function(res){
                exerciseChannel.publish('exercise.getExercises', res)
            })
        .catch(function(err){
                console.log(err);
            })
        .done();
};