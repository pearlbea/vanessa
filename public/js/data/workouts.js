/**
 * public/js/data/workouts
 */

'use strict';

var $ = require('jquery'),
    Q = require('q'),
    postal = require('postal'),
    workoutChannel = postal.channel('workout');


function activateWorkout(id, active){
    return Q($.post('/api/workouts/' + id + '/activate', {
        active: active
    }));
}

function getActiveWorkouts(){
    return Q($.get('/api/workouts/active'));
}


exports.getUserWorkouts = function(){
    return Q($.get('/api/users/' + window.userid + '/workouts'));
};

exports.getWorkout = function(id){
    return Q($.get('/api/workouts/' + id));
};


function updateWorkoutStatus(data){
    var id = window.userid;
    if(id && data) {
        return Q($.post('/api/users/' + id, data));
    }
}

function updateWorkoutExercise(data){
    var id = window.userid;
    if( id && data){
        return Q($.post('/api/users/' + id + '/workout', data));
    }
}

function assignWorkoutToUser(){
    var id = window.userid,
        data = {workoutId : '55d9c5f0a2f40fd934ca133e'};
    return Q($.post('/api/users/' + id + '/assign', data));
}

exports.getActiveWorkouts = function(){
    getActiveWorkouts()
        .then(function(res){
                workoutChannel.publish('workout.getActiveWorkouts', res)
            })
        .catch(function(err){
                console.log(err)
            })
        .done();
};


exports.activateWorkout = function(id, active){
    activateWorkout(id, active)
        .then(function(res){
            console.log(res)
        })
        .catch(function(err){
            console.log(err);
        })
        .done();
};


exports.assignWorkoutToUser = function(){
    assignWorkoutToUser()
        .then(function(res){
                console.log(res)
            })
        .catch(function(err){
                console.log(err)
            })
        .done();
};

exports.updateWorkoutStatus = function(data){

    var workout = updateWorkoutStatus(data);

    workout
        .then(function(res){
            console.log('update workout', res);
        })
        .catch(function(err){
            console.log(err);
        })
        .done();
};
/**
 * Record completed exercises in a workout
 * @param data - workoutId, exercise index
 */
exports.updateWorkoutExercise = function(data){
    updateWorkoutExercise(data)
        .then(function(res){
                console.log(res);
            })
        .catch(function(err){
                console.log(err);
            })
        .done();
};