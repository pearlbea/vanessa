/**
 * public/js/components/Routes.jsx
 **/

var React = require('react'),
    ReactRouter = require('react-router'),
    Router = ReactRouter.Router,
    Route = ReactRouter.Route,
    Link = ReactRouter.Link,
    Main = require('./components/Main.jsx'),
    WorkoutList = require('./components/WorkoutList.jsx'),
    ActiveWorkout = require('./components/ActiveWorkout.jsx'),
    Profile = require('./components/Profile.jsx'),
    More = require('./components/More.jsx');

     React.render((
         <Router>
             <Route path="/" component={Main} >
                 <Route path="workouts" component={WorkoutList} />
                 <Route path="workout/:id" component={ActiveWorkout} />
                 <Route path="profile" component={Profile} />
                 <Route path="more" component={More} />
             </Route>
         </Router>
     ), document.getElementById('container'));



