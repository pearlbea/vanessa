/**
 * @module public/js/components/WorkoutList.jsx
 */

'use strict';

var React = require('react'),
    Reflux = require('reflux'),
    Actions = require('../actions'),
    WorkoutStore = require('../stores/workout-store'),
    WorkoutDetail = require('./WorkoutDetail.jsx'),
    Router = require('react-router'),
    Link = Router.Link;

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(WorkoutStore, 'onChange')
    ],

    getInitialState: function(){
        return {
            workouts: []
        }
    },

    componentDidMount: function(){
        Actions.getUserWorkouts();
    },

    onChange: function(event, workouts){
        this.setState({
            workouts: workouts.workouts
        });
    },

    render : function () {

        var weeklyworkouts = this.state.workouts.map(function (workout, index) {

            return <WorkoutDetail key={index}
                            workout={workout}
                            index={index}
                />;

        }.bind(this));

        return(
            <div className="workouts">
                <div className="header">Workouts for 10/4 - 10/10</div>
                {weeklyworkouts}
            </div>
        );
    }
});