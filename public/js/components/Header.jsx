'use strict';

var React = require('react'),
    Menu = require('./Menu.jsx'),
    Reflux = require('reflux'),
    Actions = require('../actions'),
    WorkoutStore = require('../stores/workout-store');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(WorkoutStore, 'onChange')
    ],

    getInitialState: function(){
        return {
            email: ''
        }
    },

    onChange: function(event, workouts){
        this.setState({
            email: workouts.email,
            isAdmin: workouts.isAdmin
        });
    },

    render: function(){

        return(
            <header>
                <div>
                   <ul className="logo">
                       <li><img src="../../img/vanessa_3.png" alt="Vanessa" className="face" /></li>
                       <li><img src="../../img/Vanessa-type-beta.png" alt="Vanessa" className="type" /></li>
                   </ul>
                </div>
                <div>
                    <Menu isAdmin={this.state.isAdmin} />
                    <div className="user">{this.state.email? this.state.email: null}</div>
                </div>
            </header>
        );
    }

});