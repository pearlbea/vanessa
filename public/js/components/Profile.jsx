'use strict'

var React = require('react'),
    Reflux = require('reflux'),
    Actions = require('../actions'),
    WorkoutStore = require('../stores/workout-store'),
    WorkoutDetail = require('./WorkoutDetail.jsx');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(WorkoutStore, 'onChange')
    ],

    getInitialState: function(){
        return {
            workouts: []
        }
    },

    componentWillMount: function(){
        Actions.getUserWorkouts();
    },

    onChange: function(event, workouts){
        this.setState({
            workouts: workouts.workouts
        });
    },

    render: function(){

        var weeklyworkouts = this.state.workouts.map(function (workout, index) {

            return <div>{workout}</div>;
        }.bind(this));
        return(
            <div>

            </div>
        );
    }
});