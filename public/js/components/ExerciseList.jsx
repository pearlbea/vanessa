/**
 * @module public/js/components/ExerciseList.jsx
 */

var React = require('react'),
    Exercise = require('./Exercise.jsx');

'use strict';

module.exports = React.createClass({

    propTypes: {
        exerciseIndex: React.PropTypes.number,
        exercises: React.PropTypes.array
    },

    render: function(){

        var active = false, exercises;

        if(this.props.exercises) {
            exercises = this.props.exercises.map(function (exercise, index) {
                return <Exercise key={index} index={index} exercise={exercise} active={active}/>;
            }.bind(this));
        }

        return(
            <div className="exercise-list">
                <ul>
                    {exercises}
                </ul>
            </div>
        );
    }
});