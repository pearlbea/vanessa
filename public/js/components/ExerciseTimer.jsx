/**
 * @module public/js/components/ExerciseTimer.jsx
 */

var React = require('react'),
    $ = require('jquery'),
    PrepTimer = require('./PrepTimer.jsx'),
    ExerciseCountdown = require('./ExerciseCountdown.jsx'),
    postal = require('postal'),
    timerChannel = postal.channel('timer');

module.exports = React.createClass({

    propTypes: {
        exercise: React.PropTypes.object
    },

    getInitialState: function(){
        return{
            prep: true
        }
    },

    componentDidMount: function(){
        this.subscribeToTimer();
    },

    subscribeToTimer: function(){
        timerChannel.subscribe('timer.*', function(data, env){
            if(env.topic === 'timer.exerciseDone'){
                this.setState({
                    prep: true
                });
            }
        }.bind(this));
    },

    startExercise: function(){
        this.setState({
            prep: false
        });
    },

    render: function(){

        return(
            <div>
                {this.state.prep ?
                    <PrepTimer startExercise={this.startExercise} />
                :
                    <ExerciseCountdown
                        data={this.props.exercise}
                    />
                }
            </div>
        );
    }

});