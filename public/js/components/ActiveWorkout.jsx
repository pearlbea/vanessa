'use strict';

var React = require('react/addons'),
    ReactCSSTransitionGroup = React.addons.CSSTransitionGroup,
    Reflux = require('reflux'),
    Actions = require('../actions'),
    WorkoutStore = require('../stores/workout-store'),
    ExerciseList = require('./ExerciseList.jsx'),
    ExerciseTimer = require('./ExerciseTimer.jsx'),
    data = require('../data/workouts'),
    postal = require('postal'),
    timerChannel = postal.channel('timer'),
    $ = require('jquery');

module.exports = React.createClass({

    mixins: [
        Reflux.listenTo(WorkoutStore, 'onChange')
    ],

    getInitialState: function(){
        return {
            exerciseIndex: 0,
            workout: [],
            complete: false
        }
    },

    componentDidMount: function(){
        Actions.getWorkout(this.props.params.id);
        this.subscribeToTimer();
    },

    onChange: function(event, workout){
        this.setState({
            workout: workout
        });
    },

    subscribeToTimer: function(){
        timerChannel.subscribe('timer.*', function(data, env){
            if(env.topic === 'timer.exerciseDone'){
                this.incrementExercise();
            }
        }.bind(this));
    },

    incrementExercise: function(){

        var styles = {
            fontSize: 0,
            margin: 0,
            padding: 0,
            height: 0
        };

        $('li[data-index="' + this.state.exerciseIndex + '"]').removeClass('show').css(styles)

        $('li[data-index="' + (this.state.exerciseIndex + 1)  + '"]').addClass('active');

        data.updateWorkoutExercise({
            workoutId: this.state.workout._id,
            index: this.state.exerciseIndex
        });

        if(!this.isWorkoutComplete()) {
            this.setState({
                exerciseIndex: this.state.exerciseIndex + 1
            });
        }else{
            this.finishWorkout();
        }

    },

    isWorkoutComplete: function() {
        return Number(this.state.workout.workoutId.exercises.length) <= Number(this.state.exerciseIndex + 1);
    },

    finishWorkout: function(){
        $('li[data-index="' + this.state.exerciseIndex + '"]').removeClass('show').css('height', '0');

        data.updateWorkoutStatus({
            workoutId: this.state.workout._id,
            status: 'complete'
        });

        this.setState({
            complete: true
        });

    },

    render: function(){

        return(

            <div className="workouts active">
                {this.state.workout.workoutId?
                    <div>
                    {this.state.complete ?
                        <div className="timer complete">
                            <p>Workout completed</p>
                            <div className="countdown">Nice Job!</div>
                            <button className="button"><a href="/home">Return to feed</a></button>
                        </div>

                        :

                        <div>
                            {this.state.workout.workoutId.name}
                            <ExerciseTimer
                                exercise={this.state.workout.workoutId.exercises[this.state.exerciseIndex]}
                            />
                            <ExerciseList
                                activeIndex={this.state.exerciseIndex}
                                exercises={this.state.workout.workoutId.exercises} />
                        </div>
                    }
                    </div>
                : null}
            </div>
        );
    }
});