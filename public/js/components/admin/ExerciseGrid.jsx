/**
 * components/admin/ExerciseGrid.jsx
 */

'use strict';

var React = require('react');

module.exports = React.createClass({

    propTypes: {
        data: React.PropTypes.array
    },

    render: function(){

        var exercises = this.props.data.map(function(exercise, index){
            return <tr key={index}>
                <td>{exercise.name}</td>
                <td>{exercise.type}</td>
            </tr>;
        });

        return(
            <div className="exercise-grid">
                <table>
                    <thead>
                        <th>Exercise</th>
                        <th>Type</th>
                    </thead>
                {exercises}
                </table>
            </div>
        );
    }
});