/**
 * components/admin/WorkoutBuilder.jsx
 */
'use strict';

var React = require('react'),
    exercises = require('../../data/exercises'),
    postal = require('postal'),
    exerciseChannel = postal.channel('exercise'),
    ExerciseGrid = require('./ExerciseGrid.jsx');

module.exports = React.createClass({

    getInitialState: function(){
        return{
            exercises: []
        };
    },

    componentWillMount: function(){
        this.addListeners();
        exercises.getExercises();
    },

    addListeners: function(){
        exerciseChannel.subscribe('exercise.getExercises', function(data){
            this.setState({
                exercises: data
            })
        }.bind(this));
    },

    render: function(){
        return(
            <div>
                WorkoutBuilder
                <ExerciseGrid data={this.state.exercises} />
            </div>

        );
    }


});