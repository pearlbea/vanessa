'use strict';

var React = require('react'),
    $ = require('jquery'),
    timer = require('../timer');

module.exports = React.createClass({

    getInitialState: function(){
        return{
            paused: false
        };
    },

    componentDidMount: function(){
        this.startExercise();
    },

    componentWillUnmount: function(){
        timer.stopExerciseTimer();
    },

    pauseExercise: function(e) {
        var $target = $(e.target);
        if ($target.text() === 'Pause') {
            $target.text('Resume').css('opacity', 0.5);
        } else {
            $target.text('Pause').css('opacity', 1);
        }
        this.state.paused ? this.setState({ paused : false }) : this.setState({paused : true});
        timer.togglePauseExerciseTimer();
    },

    startExercise: function(){
        var timePerRep = this.props.data.exercise.timePerRep || 1;
        timer.startExerciseTimer({
            measure:this.props.data.measure,
            value: this.props.data.value,
            timePerRep: timePerRep
        });
    },

    render: function(){

        var style = {
            display: 'block',
            fontSize: '.8rem',
            color: '#fff'
        };

        return(
            <div className="timer exercise-countdown">
                <p>
                    <button className="button pause" onClick={this.pauseExercise}>Pause</button>

                </p>
                {this.state.paused? <a href="/" style={style}>Return to Feed</a>: null}

                <div className="countdown"></div>
            </div>
        )
    }

});
