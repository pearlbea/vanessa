/**
 * @module public/js/components/Workout.jsx
 */

'use strict';

var React = require('react'),
    Reflux = require('reflux'),
    WorkoutStore = require('../stores/workout-store'),
    _ = require('underscore'),
    data = require('../data/workouts'),
    ExerciseList = require('./ExerciseList.jsx'),
    Router = require('react-router'),
    Link = Router.Link;

module.exports = React.createClass({

    propTypes: {
        index: React.PropTypes.number,
        workout : React.PropTypes.object
    },

    getInitialState: function(){
        return{
            active: false,
            showExercises: false,
            prep: true,
            done: false
        }
    },

    componentDidMount: function(){
        this.addListeners();
    },

    getWorkoutTime: function(){
        var totalSecs = 0;
        _.each(this.props.workout.workoutId.exercises, function(item){
            if(item.measure === 'secs'){
                totalSecs += item.value;
            }else if(item.exercise.timePerRep){
                totalSecs += (item.exercise.timePerRep * item.value);
            }else{
                totalSecs += (5 * item.value);
            }
        });

        return Math.floor(totalSecs/60);
    },


    toggleWorkout: function(){
        this.state.showExercises ?
            this.setState({ showExercises: false }) :
            this.setState({ showExercises: true })
    },

    render: function(){

        console.log(this.props);

        var exercises,
            completed = this.props.workout.status === 'completed' ? true : false,
            preview = this.state.showExercises? 'preview-wrapper show' : 'preview-wrapper',
            equipment = [],
            active = this.state.active? 'active' : '';


        if(this.props.workout.workoutId.type === 'weighted'){

            var needed = this.props.workout.workoutId.exercises.map(function(exercise, index){

                if(exercise.exercise.equipment && equipment.indexOf(exercise.exercise.equipment) === -1){
                    equipment.push(exercise.exercise.equipment);
                    return  <li key={index}>{exercise.exercise.equipment} needed</li>;
                }
            });
        }

        return(
            <div ref="workout" data-index={this.props.index+1} className={'workout ' + active}>
                <div className="title">
                    {this.props.workout.workoutId.name}: <span className="workoutType">{this.props.workout.type}</span>
                    {this.props.workout.type === 'weighted' ? <img src="../img/workouts_icon3.png"/>: null}

                    <ul className="workoutDetails unstyled">
                        <li className="first">Approx. {this.getWorkoutTime()} minutes</li>
                        {needed? needed : null}
                    </ul>

                </div>

                {completed ?
                    <div className="arrow">
                        <img src="../img/checkmark.png" />
                    </div>
                    :
                    <div className="arrow" onClick={this.toggleWorkout}>
                        <img src={this.state.showExercises? "../img/arrow_up.png" : "../img/arrow_down.png"}/>
                    </div>
                }

                {completed? <div className="status">Completed</div> : null}
                    <div className={preview}>
                        <ul className="preview" >
                            <li className="start">

                                <Link to={'/workout/' + this.props.workout._id}>
                                    <button className="button">Start Workout</button>
                                </Link>
                            </li>
                            <li>
                        <ExerciseList
                            exercises={this.props.workout.workoutId.exercises} />

                            </li>
                        </ul>
                    </div>
            </div>
        );
    }
});