/**
 * @module public/js/components/PrepTimer.jsx
 */

var React = require('react');

module.exports = React.createClass({

    getInitialState: function(){
        return{
            timeLeft: 3,
            item: 1
        }
    },

    countdown: ['', 'Ready', 'Set', 'Go!'],

    componentDidMount: function() {
        this.interval = window.setInterval(this.tick, 2000);
    },

    componentWillUnmount: function() {
        clearInterval(this.interval);
    },

    tick: function() {

        this.setState({
            timeLeft: this.state.timeLeft - 1,
            item: this.state.item + 1
        });

        if (this.state.timeLeft <= 0) {
            clearInterval(this.interval);
            this.props.startExercise();
        }
    },

    render: function(){
        return(
            <div className="timer prep-timer">
                <p>Prepare for exercise</p>
                <p ref="countdown" className="countdown">
                {this.countdown[this.state.item]}
                </p>
            </div>
        );
    }

});