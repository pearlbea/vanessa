'use strict';

var React = require('react'),
    Router = require('react-router'),
    Link = Router.Link;

module.exports = React.createClass({

    render: function(){
        return(
            <ul className="menu">
                <li>
                    <Link activeClassName="active" to="/profile"><i className="icon-profile_icon"></i></Link>
                    <div className="label">Profile</div>
                </li>
                <li><Link activeClassName="active" to="/workouts"><i className="icon-workouts_icon"></i></Link>
                    <div className="label">Workouts</div>
                </li>
                <li><Link activeClassName="active" to="/more"><i className="icon-more_icon"></i></Link>
                    <div className="label">More</div>
                </li>
                {this.props.isAdmin? <li><a href="/admin">ADMIN</a></li>: null}
            </ul>
        );
    }
});



