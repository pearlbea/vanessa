/**
 * @module public/js/components/Main.jsx
 */

'use strict';

var React = require('react'),
    WorkoutList = require('./WorkoutList.jsx'),
    Header = require('./Header.jsx'),
    Footer = require('./Footer.jsx'),
    Menu = require('./Menu.jsx');

module.exports = React.createClass({

    content: function(){
        if(this.props.children){
            return this.props.children;
        }
        return <WorkoutList  />;
    },

    render: function(){
        return(
            <div>
                <Header />
                <div id="main">
                    {this.content()}
                </div>
                <Footer />
            </div>
        );
    }
});