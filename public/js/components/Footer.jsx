'use strict';

var React = require('react'),
    Menu = require('./Menu.jsx');

module.exports = React.createClass({


    render: function(){

        return(
            <div className="footer">
                <Menu  />
            </div>
        );
    }

});