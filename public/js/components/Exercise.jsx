/**
 * @module public/js/components/Exercise.jsx
 */

var React = require('react');

'use strict';

module.exports = React.createClass({

    render: function () {

        var className = this.props.active? 'exercise show active': 'exercise show';
        return(
            <li data-index={this.props.index} className={className}>
                {this.props.exercise.exercise.name},&nbsp;
                {this.props.exercise.value} {this.props.exercise.measure}
            </li>
        );
    }
});


