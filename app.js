/*jslint node: true */

"use strict";

var express = require('express'),
    path = require('path'),
    favicon = require('serve-favicon'),
    morgan = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    flash = require('connect-flash'),
    passport = require('passport'),
    session = require('express-session'),

    /* config */
    db = require('./config/db'),

    /* models */
    exercise = require('./model/exercises'),
    workout = require('./model/workouts'),
    user = require('./model/users'),
    tag = require('./model/tags'),

    /* routes */
    routes = require('./routes/index'),
    exercises = require('./routes/api/exercises'),
    workouts = require('./routes/api/workouts'),
    users = require('./routes/api/users'),
    admin = require('./routes/admin'),
    app = express();

require('./config/passport')(passport);

// don't advertise that this is express
app.disable("x-powered-by");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.png'));

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: '8m7DdFSLWB'
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/api/exercises', exercises);
app.use('/api/workouts', workouts);
app.use('/api/users', users);
app.use('/admin/', admin);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
