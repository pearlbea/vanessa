var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    mongoose = require('mongoose'); //mongo connection

'use strict';

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect('/login');
}

router.use(function(req, res, next){
    res.locals.currentUser = req.user;
    res.locals.errors = req.flash('error');
    res.locals.infos = req.flash('info');
    next();
});

router.get('/', isLoggedIn, function (req, res, next) {
    res.render('index', {title: 'Vanessa | Workouts'});
});

router.route('/login')
    .get(function (req, res) {
        res.render('login', {title: 'Vanessa | Login'});
    })
    .post(passport.authenticate('login', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    })
);

router.post('/register', isLoggedIn, passport.authenticate('signup', {
        successRedirect: '/admin',
        failureRedirect: '/admin',
        failureFlash: true
    })
);

router.get('/logout', isLoggedIn, function(req, res){
    req.logout();
    res.redirect('/login')
});

module.exports = router;
