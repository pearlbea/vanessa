/**
 * @module routes/admin
 * @type {*|exports|module.exports}
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

'use strict';

function isAdmin(req, res, next){
    if(req.isAuthenticated() && req.user.isAdmin === true){
        return next();
    }
    res.redirect('/');
}

router.use(function(req, res, next){
    res.locals.currentUser = req.user;
    res.locals.errors = req.flash('error');
    res.locals.infos = req.flash('info');
    next();
});

router.get('/', isAdmin, function (req, res) {
    res.render('admin/index', {title: 'Vanessa | Admin'});
});

router.get('/users', isAdmin, function(req, res) {
    mongoose.model('User')
        .find()
        .sort({ createdAt: 'descending'})
        .exec(function(err, users){
            if(err){
                return console.err(err);
            }else{
                res.render('admin/users/index', {title: 'Users', users: users});
            }
        });
});

router.get('/users/new', isAdmin, function(req, res){
    res.render('admin/users/new', {title: 'Add New User'});
});

router.get('/users/:id', isAdmin, function(req, res){
var workouts = '';
    mongoose.model('User')
        .getUserById(req.id)
        .exec(function (err, user) {
            var options = {
                path: 'exercises.exercise',
                model: 'Exercise'
            };
            mongoose.model('Workout').populate(workouts, options, function(){
                if (err) {
                    return console.err(err);
                }
                res.render('admin/users/view', {user: user, workouts: workouts})
            })
        });

});


/** EXERCISES **/

router.get('/exercises', isAdmin, function(req, res){
    mongoose.model('Tag').find().sort({'tag': 'ascending'}).exec(function(err, tags) {
        mongoose.model('Exercise')
            .find()
            .sort({'name': 'ascending'})
            .exec(function (err, exercises) {
                if (err) {
                    return console.err(err);
                } else {

                    res.render('admin/exercises/index', {
                        title: 'Exercises',
                        exercises: exercises,
                        tags: tags
                    });
                }
            });
    });
});

router.get('/exercises/new', isAdmin, function(req, res){
    res.render('admin/exercises/new', {title: 'Add New Exercise'})
});

router.param('id', function(req, res, next, id){
    mongoose.model('Exercise').findById(id, function(err, exercise){
        if(err){
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                },
                json: function(){
                    res.json({message : err.status  + ' ' + err});
                }
            });
        }else{
            req.id = id;
            next();
        }
    });
});
router.get('/exercises/:id/edit', isAdmin, function(req, res){

    mongoose.model('Exercise').findById(req.id, function (err, exercise) {
        if (err) {
            console.log('error ', err);
        } else {
            res.render('admin/exercises/edit', {
                title: 'Edit Exercise',
                exercise: exercise
            })
        }
    });

});

/** WORKOUTS **/

router.get('/workouts', isAdmin, function(req, res){
    mongoose.model('Workout')
        .find()
        .lean()
        .exec(function (err, workouts) {
            var options = {
                path: 'exercises.exercise',
                model: 'Exercise',
                select: 'name'
            };
            mongoose.model('Workout')
                .populate(workouts, options,  function(){
                    if (err) {
                        return console.err(err);
                    }
                res.render('admin/workouts', {title: 'Workouts', workouts: workouts })
            })
        });
});

router.get('/workouts/edit/:id', isAdmin, function(req, res){

    mongoose.model('Exercise').find().sort({'name': 'ascending'}).exec(function(err, exerciseList){
        mongoose.model('Workout').findById(req.id, function(err, workout){

                var options = {
                    path: 'exercises.exercise',
                    model: 'Exercise',
                    select: 'name'
                };

                mongoose.model('Workout')
                    .populate(workout, options,  function(){
                        if (err) {
                            return console.err(err);
                        }
                        res.render('admin/workouts/edit', {title: 'Workout', workout: workout, exerciseList: exerciseList })
                    })
        })
    })
});

router.get('/workouts/new', isAdmin, function (req, res) {
    mongoose.model('Exercise')
        .find()
        .sort({'name': 'ascending'})
        .exec(function(err, exercises){
            if(err){
                return console.err(err);
            }else{
                res.render('admin/workouts/new', {title: 'Add Workout', exercises: exercises });
            }
        });

});

module.exports = router;