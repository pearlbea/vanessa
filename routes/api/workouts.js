/**
 * routes/workout
 */
var express = require('express'),
    _ = require('underscore'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

"use strict";

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function (req, res) {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));

router.route('/')

// Get all workouts
    .get(function (req, res) {
        mongoose.model('Workout')
            .find()
            .sort({'name': 'ascending'})
            .exec(function (err, workouts) {
                var options = {
                    path: 'exercises.exercise',
                    model: 'Exercise'
                };
                mongoose.model('Workout').populate(workouts, options, function(){
                    if (err) {
                        return console.err(err);
                    }
                    res.json(workouts);
                })
            });
    })

// Add a new workout
    .post(function (req, res) {

        var name = req.body.name,
            type = req.body.type,
            level = req.body.level;

        mongoose.model('Workout').create({
            status: 'not started',
            name: name,
            type: type,
            level: level,
            exercises: [
                {exercise: req.body.exercise1, measure: req.body.exercise1_measure, value: req.body.exercise1_value},
                {exercise: req.body.exercise2, measure: req.body.exercise2_measure, value: req.body.exercise2_value},
                {exercise: req.body.exercise3, measure: req.body.exercise3_measure, value: req.body.exercise3_value},
                {exercise: req.body.exercise4, measure: req.body.exercise4_measure, value: req.body.exercise4_value},
                {exercise: req.body.exercise5, measure: req.body.exercise5_measure, value: req.body.exercise5_value}
            ]
            }, function (err, workout) {
                if (err) {
                    res.send('Oops. We couldn\'t add this workout', err);
                } else {
                    res.json(workout);
                }
        });
    });


router.get('/active', function(req, res){
    mongoose.model('Workout')
        .find({active: true})
        .exec(function (err, workouts) {
            var options = {
                path: 'exercises.exercise',
                model: 'Exercise'
            };
            mongoose.model('Workout').populate(workouts, options, function(){
                if (err) {
                    return console.err(err);
                }
                res.json(workouts);
            })
        });
});



router.param('id',function(req, res, next, id){
    mongoose.model('Workout').findById(id, function (err, workout){
        if(err){
            console.log(err);
        }else{
            req.id = id;
            next();
        }
    })
});

router.route('/:id')
    .get(function(req, res){
        mongoose.model('Workout')
            .getWorkoutById(req.id)
            .exec(function (err, workout) {
                if (err) {
                    return console.err(err);
                }
                res.json(workout);
            });
    });

router.route('/:id/edit')
    .post(function(req, res){

        mongoose.model('Workout')
            .findById(req.id, function(err, workout){
                if(err){
                    console.log(err);
                }
                workout.name = req.body.name;
                workout.type = req.body.type;
                workout.level = req.body.level;
                workout.exercises = [];

                _.each(req.body.exerciseId, function(id, index){
                    workout.exercises.push({
                        exercise: id,
                        measure: req.body.measure[index],
                        value: req.body.value[index]
                    });
                });

                workout.save(res.redirect('/admin/workouts'));
            });
});



router.post('/:id/activate', function(req, res){
    mongoose.model('Workout')
        .findByIdAndUpdate(req.id, {active: req.body.active}, function(err, workout){
        if(err){
            console.log(err);
        }
        res.json(workout);
    });
});

router.param('week', function(req, res, next, week){
    mongoose.model('Workout')
        .find({ week : week }, function(err){
            if(err){
                console.log(err)
            }else{
                req.week = week;
                next();
            }
        })
});

router.route('/week/:week')

    .get(function (req, res) {
        mongoose.model('Workout')
            .find({ week: req.week })
            .exec( function(err, workout){
                var options = {
                    path: 'exercises.exercise',
                    model: 'Exercise'
                };

                mongoose.model('Workout').populate(workout, options, function(){
                    if (err) {
                        return console.err(err);
                    }
                    res.json(workout);
                })

            });
});

router.param('type', function(req, res, next, type){
    mongoose.model('Workout')
        .find({ type : type }, function(err){
            if(err){
                console.log(err)
            }else{
                req.type = type;
                next();
            }
        })
});

router.route('/type/:type')
    .get(function (req, res) {
        mongoose.model('Workout')
            .find({type: req.type})
            .exec(function (err, workout) {
                var options = {
                    path: 'exercises.exercise',
                    model: 'Exercise'
                };

                mongoose.model('Workout').populate(workout, options, function () {
                    if (err) {
                        return console.err(err);
                    }
                    res.json(workout);
                })

            });
    });

module.exports = router;