var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    passport = require('passport');

"use strict";

// gets list of all users
router.get('/', function(req, res) {
    mongoose.model('User')
        .find()
        .select('email')
        .sort({ createdAt: 'descending'})
        .exec(function(err, users){
            if(err){
                return console.err(err);
            }else{
                res.json(users);
            }
        });
    });

router.param('id',function(req, res, next, id){
    mongoose.model('User')
        .findById(id, 'email', function (err, user){
            if(err){
                console.log(err);
            }else{
                req.id = id;
                next();
            }
    })
});

// get one user by id
router.route('/:id')
    .get(function(req, res){
        mongoose.model('User')
            .getUserById(req.id)
            .exec(function (err, user) {
                if (err) {
                    return console.err(err);
                }
                res.json(user);
            })
    })
    .post(function(req, res){
        mongoose.model('User')
            .updateWorkoutStatus(req.id, req.body)
            .exec(function(err, user){
                if(err){
                    return console.log(err);
                }
                res.json(user);
            })
    });

// assign workout to user
router.route('/:id/assign')
    .post(function(req, res){
        mongoose.model('User')
            .assignWorkoutToUser(req.id, req.body)
            .exec(function(err, user){
                console.log(req)
                if(err){
                    return console.log(err);
                }

                console.log(req.body)
            });
    });



router.route('/:id/workout')
    .post(function(req, res){
        mongoose.model('User')
            .updateWorkoutExercise(req.id, req.body)
            .exec(function(err, user){
                if(err){
                    return console.log(err);
                }
                res.json(user);
            })
    });



// get all workouts for one user
router.route('/:id/workouts')
    .get(function(req, res){
        mongoose.model('User')
            .getUserById(req.id)
            .exec(function (err, user) {
                if (err) {
                    return console.log(err);
                }
                mongoose.model('User')
                    .populate(user, {
                        path: 'workouts.workoutId.exercises.exercise',
                        model: 'Exercise'

                }, function(){
                        if (err) {
                            return console.err(err);
                        }
                        res.json(user);
                    })
            });
    });


router.get('/:id/edit', function(req, res){
    mongoose.model('Workout')
        .find()
        .exec(function(err, workouts){
            if(err){
                return console.err(err);
            }else{
                res.render('admin/users', {title: 'Edit User', workouts: workouts });
            }
        });

});

module.exports = router;
