/**
 * Created by pearllatteier on 7/26/15.
 */
var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'); //used to manipulate POST

"use strict";

router.use(bodyParser.urlencoded({ extended: true }));

router.use(methodOverride(function(req, res){
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
    }
}));

// Get all exercises
router.route('/')
    .get(function(req, res, next){
        mongoose.model('Exercise')
            .find()
            .sort({'name': 'ascending'})
            .exec(function(err, exercises){
                if(err){
                    return console.err(err);
                }else{
                    res.json(exercises);
                }
        });
    })
    .post(function(req, res){
        var name = req.body.name,
            type = req.body.type,
            timePerRep = req.body.timePerRep,
            equipment = req.body.equipment;

        mongoose.model('Exercise').create({
            name: name,
            type: type,
            timePerRep: timePerRep,
            equipment: equipment
        }, function(err, exercise){
            if(err){
                res.send('Oops. We couldn\'t add this exercise');
            }else{
                res.json(exercise);
            }
        })
    });

router.param('id', function(req, res, next, id){
    mongoose.model('Exercise').findById(id, function(err, exercise){
        if(err){
            console.log(id + ' was not found');
            res.status(404)
            var err = new Error('Not Found');
            err.status = 404;
            res.format({
                html: function(){
                    next(err);
                },
                json: function(){
                    res.json({message : err.status  + ' ' + err});
                }
            });
        }else{
            req.id = id;
            next();
        }
    });
});

router.route('/:id')
    .get(function (req, res) {
        mongoose.model('Exercise').findById(req.id, function (err, exercise) {
            if (err) {
                console.log('error ', err);
            } else {
                res.json(exercise);
            }
        });
    });

router.route('/:id/edit')
    .post(function(req, res){
        console.log(req.body)
        mongoose.model('Exercise')
            .findByIdAndUpdate(req.id, {
                name: req.body.name,
                type: req.body.type,
                timePerRep: req.body.timePerRep,
                equipment: req.body.equipment,
                tags: req.body.tags
            },
            {safe: true, upsert: true},
            function(err){
                if(err){
                    return console.log(err);
                }
                res.redirect('/admin/exercises');
            });
    });




module.exports = router;