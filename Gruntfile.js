/**
 * Created by pearllatteier on 7/26/15.
 */
/*jslint
 browser : true, node: true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */

module.exports = function(grunt){

    'use strict';

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        watch : {
            browserify : {
                files : ['public/js/**/*.js', 'public/js/**/*.jsx'],
                tasks : ['browserify']
            },

            sass : {
                files: 'public/style/*',
                tasks: ['sass']
            }
        },

        browserify : {
            app : {
                src: ['public/js/*.js', 'public/js/**/*.jsx'],
                dest: 'public/dist/main.js',
                options: {
                    watch: true,
                    transform: ['reactify']
                }
            }
        },

        uglify : {
            my_target : {
                files : {
                    'public/dist/main.min.js' : ['public/dist/main.js']
                }
            }
        },

        sass : {
            dist : {
                options : {
                    style: 'compressed'
                },
                files : {
                    'public/dist/style.css' : 'public/style/style.scss',
                    'public/dist/admin.css' : 'public/style/admin.scss'
                }
            }
        },


        clean : {
            dist : ['public/dist']
        }

    });
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default',['clean', 'browserify', 'sass']);



};