var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

'use strict';

var workoutSchema = new Schema({
    type: {type: String, enum: ['bodyweight', 'weighted']},
    name: {type: String},
    created_at: { type: Date, default: Date.now },
    level: Number, // 1 - easy; 2 - moderate; 3 - hard
    tags: [{type: Schema.Types.ObjectId, ref: 'Tag'}],
    active: {type :Boolean, default: false},
    exercises: [{
        exercise: {type: Schema.Types.ObjectId, ref: 'Exercise'}, // refers to Exercise model
        measure: {type: String}, // reps or secs
        value: {type: Number} // number of reps or secs
    }]
});

workoutSchema.statics.getWorkoutById = function(id){
    return this.findById(id)
        .populate('exercises.exercise', null, null, options);
};

var Workout = mongoose.model('Workout', workoutSchema);

module.exports = Workout;
