var mongoose = require('mongoose');

"use strict";

var tagSchema = new mongoose.Schema({
    tag: { type: String, required: true, unique: true }
});

var Tag = mongoose.model('Tag', tagSchema);

module.exports = Tag;
