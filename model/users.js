/* model/users.js */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt-nodejs');

"use strict";

var userSchema = new Schema({
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    isAdmin: {type: Boolean, default: false},
    createdAt: {type: Date, default: Date.now},
    level:  Number,
    workouts: [{
        workoutId: {type: Schema.Types.ObjectId, ref: 'Workout'},
        status: {type: String, default: 'Assigned'}, // not started, started, completed
        completedExercises : [Number], // stores indices of completed exercises
        date: {type: Date, default: Date.now}
    }]
});

userSchema.statics.getUserById = function(id){
    var options = {sort: 'name'};
    return this.findById(id)
        .populate('workouts.workoutId', null, null, options);
};

userSchema.statics.assignWorkoutToUser = function(id, data){
    return this.findOneAndUpdate({'_id': id},
        {$push: {'workouts': {'workoutId': data.workoutId}} });
};

userSchema.statics.updateWorkoutStatus = function(id, data){
    return this.findOneAndUpdate({'_id' : id, 'workouts.workoutId': data.workoutId },
        { $set: { 'workouts.$.status': data.status } });
};

userSchema.statics.updateWorkoutExercise = function(id, data){
    return this.findOneAndUpdate({'_id' : id, 'workouts.workoutId': data.workoutId },
        { $push: { 'workouts.$.completedExercises': data.index } });
};

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

var User = mongoose.model('User', userSchema);


module.exports = User;
