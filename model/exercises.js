var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

"use strict";

var exerciseSchema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    type: { type: String, enum: ['bodyweight', 'weighted']},
    timePerRep: { type: Number }, // in seconds
    equipment: { type: String },
    tags: {type: String}
});

exerciseSchema.statics.getExercisesByType = function(type){
    return this.find({ type: type });
};

exerciseSchema.statics.getExercisesByTag = function(tag){
    return this.find().where(tag).in(tags);
};

var Exercise = mongoose.model('Exercise', exerciseSchema);

module.exports = Exercise;
