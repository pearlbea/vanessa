/**
 * Created by pearllatteier on 8/23/15.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('../model/users');

'use strict';

module.exports = function() {

    passport.serializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    // Registration authentication
    passport.use('signup', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {

            process.nextTick(function () {
                User.findOne({'email': email}, function (err, user) {

                    if (err) {
                        return done(err);
                    }

                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        var newUser = new User();

                        newUser.email = email;
                        newUser.password = newUser.generateHash(password);

                        newUser.save(function (err) {
                            if (err) {
                                throw err;
                            }
                            return done(null, newUser);
                        });
                    }
                });
            });
        }));


    // Login authentication
    passport.use('login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, function(req, email, password, done) {

            User.findOne({email: email}, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false, req.flash('loginMessage', 'login error'));
                }
                if(!user.validPassword(password)){
                    return done(null, false, req.flash('loginMessage', 'login error'))
                }
                return done(null, user);
            });
        }));
};